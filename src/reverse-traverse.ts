export interface ItemReverse {
  id: string;
  children: ItemReverse[];
}

export interface Item {
  id: string;
  parent?: Item;
}

// TODO
export const reverseTraverse = (items: Item[]): ItemReverse[] => {
  return [];
};
