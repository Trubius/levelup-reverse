module.exports = {
  extends: ['@codingsans/eslint-config/typescript-recommended'],
  plugins: ['const-immutable'],
  rules: {
    complexity: ['error', 2],
    curly: 'error',
    'const-immutable/no-mutation': 2,
  },
};
